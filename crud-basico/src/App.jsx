import React from 'react';
// import ListadoNombres from './components/ListadoNombres';
// import Props from './components/PROPS/Props';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Inicio from './components/ROUTER/Inicio';
import Base from './components/ROUTER/Base';
import Usuarios from './components/ROUTER/Usuarios';
import Perfil from './components/ROUTER/Perfil';

const App = () => {
    return (
        // ROUTER
        <Router>
            <Link className='btn btn-info m-2' to='/inicio'>
                Inicio
            </Link>
            <Link className='btn btn-info m-2' to='/'>
                Base
            </Link>
            <Link className='btn btn-info m-2' to='/Usuarios'>
                Usuarios
            </Link>

            <Switch>
                <Route exact path='/'>
                    <Base></Base>
                </Route>

                <Route path='/inicio/:id/:nombre/:edad'>
                    <Inicio></Inicio>
                </Route>

                <Route path='/usuarios'>
                    <Usuarios></Usuarios>
                </Route>

                <Route path='/perfil/:id'>
                    <Perfil></Perfil>
                </Route>
            </Switch>
        </Router>

        // <div className='container'>
        //     {/* <ListadoNombres></ListadoNombres> */}

        //     {/* <h1 className='text-center bg-dark text-white my-3 py-4'>
        //         Propiedades de los Componentes
        //     </h1>

        //     <Props nombre='Luis'></Props>
        //     <Props nombre='Paco'></Props>
        //     <Props nombre='Noe'></Props>
        //     <Props nombre='Kike'></Props> */}
        // {/* </div> */}
    );
};

export default App;

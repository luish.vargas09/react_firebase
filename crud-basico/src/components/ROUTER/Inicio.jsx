import React from 'react';
import { useParams } from 'react-router-dom';

const Inicio = (props) => {
    // useParamas() devolverá un objeto con propiedades y una
    // de sus propiedades es nombre.
    console.log(useParams());

    // Hacemos un object destructuring para tener a nombre como variable local
    const { id, nombre, edad } = useParams();

    return (
        <div>
            <h1>
                RUTA INICIO <br /> "localhost:3000/inicio"
            </h1>

            <div className='card' style={{ width: '18rem' }}>
                <img className='card-img-top' src='https://' alt='profile' />
                <div className='card-body'>
                    <h5 className='card-title'>Datos del Usuario</h5>

                    <li>
                        <b>ID: </b>
                        {id}
                    </li>
                    <li>
                        <b> Nombre:</b> {nombre}
                    </li>
                    <li>
                        <b>Edad: </b>
                        {edad}
                    </li>
                </div>
            </div>
        </div>
    );
};

export default Inicio;

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router';

const Perfil = () => {
    const [Perfil, setPerfil] = useState([]);
    const { id } = useParams();

    const obtenerPerfil = async () => {
        const res = await axios.get(
            `https://jsonplaceholder.typicode.com/users/${id}`
        );
        const datos = await res.data;
        setPerfil(datos);
        console.table(datos);
    };

    useEffect(() => {
        obtenerPerfil();
    }, []);

    return (
        <div>
            <h1 className='text-center'>PERFIL</h1>
            <h2>DATOS DEL USUARIO</h2>
            <ul>
                <li>ID: {Perfil.id}</li>
                <li>Nombre: {Perfil.name}</li>
                <li>Email: {Perfil.email}</li>
                <li>Phone: {Perfil.phone}</li>
                <li>WebSite: {Perfil.website}</li>

                {/* <li>Company: {Perfil.company.name}</li>
                <ul>
                    <b>Otros Datos</b>
                    <li>{Perfil.company.catchPhrase}</li>
                    <li>{Perfil.company.bs}</li>
                </ul> */}
            </ul>
        </div>
    );
};

export default Perfil;

import React, { useState, useEffect } from 'react';

const Base = (props) => {
    const [Nombre, setNombre] = useState('Humberto');

    useEffect(() => {
        setTimeout(() => {
            setNombre('Eder');
        }, 2000);
    }, []);

    return (
        <div>
            <h1>
                RUTA BASE <br /> "localhost:3000/"
                {Nombre}
            </h1>
        </div>
    );
};

export default Base;

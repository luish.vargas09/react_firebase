import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const Usuarios = () => {
    const [usuarios, setusuarios] = useState([]);

    const obtenerUsuarios = async () => {
        const resp = await axios.get(
            'https://jsonplaceholder.typicode.com/users'
        );
        const users = await resp.data;
        setusuarios(users);
        console.table(users);
    };

    useEffect(() => {
        obtenerUsuarios();
    }, []);

    return (
        <div>
            <h1>LISTA DE USUARIOS</h1>
            {usuarios.map((item) => (
                <div>
                    <ul>
                        <Link to={`/perfil/${item.id}`}>
                            <li>
                                {item.id} - {item.name}
                            </li>
                        </Link>
                    </ul>
                </div>
            ))}
        </div>
    );
};

export default Usuarios;

import React, { useState } from 'react';
import uniqid from 'uniqid';

const ListadoNombres = () => {
    const [Nombre, setNombre] = useState('');
    const [ListaNombres, setListaNombres] = useState([]);
    const [ModoEdicion, setModoEdicion] = useState(false);
    const [Id, setId] = useState('');
    const [Error, setError] = useState(null);

    const addNombre = (event) => {
        // Esto es para que no recargue la página
        event.preventDefault();
        if (!Nombre.trim()) {
            setError('* El campo nombre es requerido');
            return;
        }
        // Generamos un objeto (LOCAL)
        const nuevoNombre = {
            id: uniqid(),
            name: Nombre,
        };
        // Actualizamos el valor del array de useState
        // setListaNombres(Nombre);
        // o si quisieramos insertar más valores al array
        setListaNombres([...ListaNombres, nuevoNombre]);
        setNombre('');
        setError(null);
    };

    const deleteNombre = (id) => {
        const nuevoArray = ListaNombres.filter((item) => item.id !== id);
        setListaNombres(nuevoArray);
    };

    const edit = (item) => {
        setModoEdicion(true);
        setNombre(item.name);
        setId(item.id);
    };

    const editNombre = (e) => {
        e.preventDefault();
        const newArray = ListaNombres.map((item) =>
            item.id === Id ? { id: Id, name: Nombre } : item
        );
        setListaNombres(newArray);
        setNombre('');
        setModoEdicion(false);
    };

    return (
        <div>
            <div className='row mt-5'>
                <h1 className='text-center text-white bg-dark'>CRUD BÁSICO</h1>

                {/* FORMULARIO DE REGISTRO */}
                <div className='col-4 bg-dark text-white'>
                    <h2 className='text-center mt-3'>FORMULARIO</h2>
                    <form
                        onSubmit={ModoEdicion ? editNombre : addNombre}
                        className='form-group mt-3'
                    >
                        <input
                            type='text'
                            placeholder='*Introduce nombre'
                            className='form-control'
                            onChange={(event) => {
                                setNombre(event.target.value);
                            }}
                            value={Nombre}
                        />

                        {Error != null ? (
                            <div
                                className='mx-2 mt-1 mb-3'
                                style={{ fontSize: '12px', color: 'red' }}
                            >
                                {Error}
                            </div>
                        ) : (
                            <div></div>
                        )}

                        <input
                            type='submit'
                            className='form-control btn-primary my-2 mb-4'
                            value={
                                ModoEdicion ? 'Confirmar edición' : 'Agregar'
                            }
                        />
                    </form>
                </div>

                {/* LISTADO DE NOMBRES */}
                <div className='col bg-dark text-white mx-2'>
                    <h2>Listado de Nombres</h2>
                    <ul className='list-group mb-3'>
                        {ListaNombres.map((item) => (
                            <li key={item.id} className='list-group-item'>
                                {item.name}

                                <button
                                    className='btn btn-danger float-end'
                                    onClick={() => {
                                        deleteNombre(item.id);
                                    }}
                                >
                                    Borrar
                                </button>

                                <button
                                    className='btn btn-info float-end mx-1 text-white'
                                    onClick={() => {
                                        edit(item);
                                    }}
                                >
                                    Editar
                                </button>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>

            <p className='mt-2 text-center font-monospace'>
                Los datos se almacenan de manera Local en un array de objetos,
                puedes notarlo en la consola o en los complementos
            </p>
        </div>
    );
};

export default ListadoNombres;

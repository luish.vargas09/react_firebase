const Persona = {
    nombre: 'Humberto',
    edad: 29,
    casado: false,
    hijo: {
        nombreHijo: 'Luisito',
    },
};

Persona.id = 1;
console.log(Persona);
console.log(Persona.nombre);
console.log(Persona.edad);
console.log(Persona.casado);

console.log(Persona.hijo.nombreHijo);

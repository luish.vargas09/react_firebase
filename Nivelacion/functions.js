// function saludar() {
//     console.log('Hola mundo cruel');
// }

// saludar();

// const saludo = () => console.log('Estoy en una función anónima');

// saludo();

// const saludar = (a, b, c) => {
//     // console.log(a);
//     // console.log(b);
//     // console.log(c);
//     return a + b + c;
// };

// // console.log(saludar(3, 'Humberto', true));
// console.log(saludar(3, 3, 2));

// Funciones dentro de funciones (curring)
const func1 = (dato) => {
    return dato;
};

const func2 = (d) => {
    console.log(d);
};

func2(func1('Humberto'));

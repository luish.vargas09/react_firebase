const datos = [
    { nombre: 'Humberto', edad: 29 },
    { nombre: 'Ana', edad: 30 },
    { nombre: 'Eder', edad: 22 },
];

// Recorre un array de objeto
datos.forEach((elemento) => {
    // console.log(elemento);
    console.log(elemento.nombre);
});

// Map es mucho más rápido (misma sintaxis)
datos.map((elemento) => {
    // console.log(elemento);
    console.log(elemento.nombre);
});

// Hago una solicitud a esa dirección (servidor)
fetch('https://jsonplaceholder.typicode.com/todos/1')
    // Entonces recibo una respuesta y la transformo a json
    .then((response) => response.json())
    // Entonces ese objeto json lo muestro en consola
    .then((json) => console.log(json.title))
    .catch((e) => console.log(e));

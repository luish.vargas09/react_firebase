const Persona = {
    nombre: 'Humberto',
    edad: 29,
    casado: false,
    hijo: {
        nombreHijo: 'Luisito',
    },
};

// Del objeto "Persona" tomo las claves de nombre y edad
const { nombre, edad } = Persona;
console.log(nombre);
console.log(edad);

// El nombre de la constante que vamos a sacar del objeto
// debe de ser igual que el declarado dentro del objeto
const { nombreHijo } = Persona.hijo;
console.log(nombreHijo);

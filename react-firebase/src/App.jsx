import React, { Fragment } from 'react';
import './App.css';
// import Contador from './components/Contador';
// import Listado from './components/Listado';
// import Temperatura from './components/Temperatura';
import Formulario from './components/Formulario';

function App() {
    // let saludo = 'hola';
    return (
        <Fragment>
            <div className='App mt-5'>
                <h1 className='mb-3'>Navegador App.jsx</h1>
                {/* <h3>{saludo}, soy un componente </h3> */}
                {/* <Contador></Contador>
                    <Listado></Listado> */}
                {/* <Temperatura></Temperatura> */}
                <Formulario></Formulario>
                <p
                    className='text-box text-secondary my-3 mx-5'
                    style={{ fontSize: '14px' }}
                >
                    Desde app.jsx importamos los componentes que queramos
                    mostrar en la página, es nuestro navegador de componentes y
                    screens
                    <br />
                    <br />
                    El formulario que se muestra para ingresar datos es un
                    componente de la página web, para acceder a el tenemos que
                    importarlo, desde su dirección, y después llamarlo en la
                    función principal de navegación de App.jsx
                </p>
            </div>
        </Fragment>
    );
}

export default App;

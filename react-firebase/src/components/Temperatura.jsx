import React, { useState } from 'react';

const Temperatura = () => {
    const [Temperatura, setTemperatura] = useState(19);

    const subir = () => {
        setTemperatura(Temperatura + 1);
    };

    const bajar = () => {
        setTemperatura(Temperatura - 1);
    };

    return (
        <div className='container'>
            <h2>La temperatura es: {Temperatura}</h2>
            <h3 className='text-info'>
                {Temperatura > 21 ? 'Hace calor' : 'Hace frío'}
            </h3>

            <div className='row'>
                <button
                    onClick={subir}
                    className='btn btn-success btn-block btn-sm my-2'
                >
                    Aumentar temperatura
                </button>
                <button
                    onClick={bajar}
                    className='btn btn-danger btn-block btn-sm my-0'
                >
                    Reducir temperatura
                </button>
            </div>
        </div>
    );
};

export default Temperatura;

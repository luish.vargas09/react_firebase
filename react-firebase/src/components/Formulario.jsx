import React, { useState } from 'react';

const Formulario = () => {
    const [Nombre, setNombre] = useState('');
    const [Edad, setEdad] = useState('');

    const validar = (event) => {
        event.preventDefault();
        console.log('Boton pulsado');

        if (!Nombre.trim()) {
            console.log('El nombre está vacío');
        }

        if (!Edad.trim()) {
            console.log('Debe colocar una edad');
        }
    };

    return (
        <div className='container'>
            <form className='form-group' onSubmit={validar}>
                <div className='row'>
                    <input
                        className='form-control mb-2'
                        type='text'
                        name=''
                        id=''
                        placeholder='Introduce nombre'
                        onChange={(e) => {
                            setNombre(e.target.value);
                        }}
                    />
                    <input
                        className='form-control mb-3'
                        type='text'
                        name=''
                        id=''
                        placeholder='Introduce edad'
                        onChange={(e) => {
                            setEdad(e.target.value);
                        }}
                    />

                    <input
                        className='btn btn-info btn-block btn-sm text-white'
                        type='submit'
                        name=''
                        id=''
                    />
                </div>
            </form>
        </div>
    );
};

export default Formulario;
